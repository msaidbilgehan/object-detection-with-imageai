# Object Detection with ImageAI

Written with Python

Tested on Python 3.5.3 and DeepinOS 15.11 Stable

Necessary dependencies will be stored in "requirements.txt" file. Can be
installed with using;

    pip install -r requirements.txt

Virtual environment is recommended.

Basically runs with given command below (place a "image.jpg" at working directory);

    python3 run.py

# References

    1) [Basic Tutorial - Object Detection with 10 Lines of Code](https://towardsdatascience.com/object-detection-with-10-lines-of-code-d6cb4d86f606)

    2) [Yolo v2 Model Training (Lang: TR)](https://medium.com/yavuzkomecoglu/yolov2-ile-kendi-%C3%B6zel-ki%C5%9Fi-yada-nesnemizin-alg%C4%B1lanmas%C4%B1n%C4%B1-nas%C4%B1l-sa%C4%9Flar%C4%B1z-b%C3%B6l%C3%BCm-2-c717f5231e46#:~:text=YOLO%20ile%20nesne%20tespiti%20yapabilmek,BBox%20Label%20Tool%20arac%C4%B1n%C4%B1%20kullan%C4%B1yoruz)

# Example

## INPUT

![INPUT](https://gitlab.com/msaidbilgehan/object-detection-with-imageai/-/raw/master/image.jpg)

## OUTPUT

 - person  :  88.81600499153137
 - person  :  91.0433828830719
 - person  :  84.83719229698181
 - person  :  95.34119963645935
 - person  :  67.88800358772278
 - person  :  90.33364653587341
 - person  :  95.46651244163513
 - person  :  65.62868356704712

![OUTPUT](https://gitlab.com/msaidbilgehan/object-detection-with-imageai/-/raw/master/imagenew.jpg)

# License

    This project is released under given license in LICENSE file.

# Contributing

    To contribute to this project please contact Muhammed Said BİLGEHAN <msaidbilgehan@gmail.com>
